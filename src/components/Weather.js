import axios from 'axios';
import React, { useState } from 'react';

export default function Weather() {
    const [dataInput, setDataInput] = useState('');
    const [weatherData, setWeatherData] = useState({
        data: {},
        error: false,
    });

    const search = async (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();

            const url = "https://api.openweathermap.org/data/2.5/weather";
            const api_key = "dfa5f0edd5283b91653961969753a612";

            await axios.get(url, {
                params: {
                    q: dataInput,
                    appid: api_key,
                    units: 'metric'
                }
            }).then((response) => {
                console.log(response);
                setWeatherData({ data: response.data, error: false });
            }).catch((error) => {
                console.log(error);
                setWeatherData({ ...weatherData, data: {}, error: true });
                setDataInput('');
            })
        }
    }

    return (
        <div className="flex justify-center items-center min-h-screen bg-gradient-to-b from-blue-400 to-blue-900">
           
            <div className="w-11/12 md:w-7/12 xl:w-4/12 mx-auto bg-white rounded-lg shadow-lg overflow-hidden">
            <h1 className='text-2xl font-semibold text-gray-800 my-3 text-center'>Climate Track</h1>
                <div className="p-4">
                    <input type="text" placeholder='Enter your city name' name="cityName" value={dataInput} onChange={(e) => setDataInput(e.target.value)} onKeyUp={search} className="w-full px-4 py-2 text-gray-700 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" />
                </div>
                <div className="p-4">
                    {weatherData.error && (<p className="text-red-500 text-center p-3 rounded-lg bg-red-50 border border-red-500">Something went wrong</p>)}
                    {weatherData && weatherData.data && weatherData.data.main && (
                        <div className="text-center mx-5">
                            <img src={`https://openweathermap.org/img/wn/${weatherData.data.weather[0].icon}.png`} alt={weatherData.data.weather[0].description} className="w-40 h-40 mx-auto" />
                            
                            <p className="text-5xl font-semibold text-gray-800">{Math.round(weatherData.data.main.temp)}°C</p>
                            <p className="text-lg font-semibold">{weatherData.data.name}, {weatherData.data.sys.country}</p>
                            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-5  my-5 *:bg-sky-50 *:rounded *:shadow-sm *:px-3 *:py-5">
                                <div>
                                <p className='text-gray-600'>Wind Speed</p>
                                <span className="text-xl font-semibold text-gray-800">{weatherData.data.wind.speed} m/s</span>
                                </div>
                                <div>
                                <p className='text-gray-600'>ATM Pressure</p>
                                <span className="text-xl font-semibold text-gray-800">{weatherData.data.main.pressure} hPa</span>
                                </div>
                                <div>
                                <p className='text-gray-600'>Humidity</p>
                                <span className="text-xl font-semibold text-gray-800">{weatherData.data.main.humidity} %</span>
                                </div>
                                <div>
                                <p className='text-gray-600'>Min. Temp</p>
                                <span className="text-xl font-semibold text-gray-800">{weatherData.data.main.temp_min} °C</span>
                                </div>
                                <div>
                                <p className='text-gray-600'>Max. Temp</p>
                                <span className="text-xl font-semibold text-gray-800">{weatherData.data.main.temp_max} °C</span>
                                </div>
                               
</div>

                        </div>
                    )}
                </div>
                <div className='text-center text-gray-500 mb-5'>
                    Designed by <a href='https://gitlab.com/er-arunkumar' className='text-medium underline text-gray-800'>Arunkumar Selvam</a>
                </div>
            </div>
        </div>
    )
}

